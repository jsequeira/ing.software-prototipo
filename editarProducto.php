<?php
include_once 'conexion.php';
$id = $_GET['id'];
$codigo = $_GET['codigo'];
$nombre = $_GET['nombre'];
$precioCompra = $_GET['precio_compra'];
$precioVenta = $_GET['precio_venta'];
$stock = $_GET['stock'];
$marca = $_GET['marca'];

if ($_GET['action'] == 'editar') {
   
    $id = $_GET['id'];
    $codigo = $_GET['codigo'];
    $nombre = $_GET['nombre'];
    $precioCompra = $_GET['precio_compra'];
    $precioVenta = $_GET['precio_venta'];
    $stock = $_GET['stock'];
    $marca = $_GET['marca'];
    $query = 'UPDATE producto SET codigo=?, nombre=?,precio_compra=?,precio_venta=?,stock=?,marca=? WHERE id = ?';
    $resultado = conexionCover()->prepare($query);
    $resultado->execute(array($codigo, $nombre,$precioCompra,$precioVenta, $stock,$marca,$id));
}

?>

<!DOCTYPE html>
<html lang="en">




<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="editarProducto.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>

<body>
    <main>

        <div class="container-form mt-1 mb-1">
            <div class="border p-5 border-primary rounded">
                <form id="myForm" onsubmit="return false">
                    <div class="form-outline mb-4">
                        <input id="id" type="text" class="form-control" name="id" value="<?php echo $id ?>" required />
                        <label class="form-label" form="form2Example2">id</label>
                    </div>

                    <div class="form-outline mb-4">
                        <input id="codigo" type="text" class="form-control" name="codigo" value="<?php echo $codigo ?>" required />
                        <label class="form-label" form="form2Example2">Codigo</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="nombre" type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
                        <label class="form-label" form="form2Example2">Nombre</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="precioCompra" type="text" class="form-control" name="precioCompra" value="<?php echo $precioCompra ?>" required />
                        <label class="form-label" form="form2Example2">Precio compra</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="precioVenta" type="text" class="form-control" name="precioVenta" value="<?php echo $precioVenta ?>" required />
                        <label class="form-label" form="form2Example2">Precio Venta</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="stock" type="text" class="form-control" name="stock" value="<?php echo $stock ?>" required />
                        <label class="form-label" form="form2Example2">Stock</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="marca" type="text" class="form-control" name="marca" value="<?php echo $marca ?>" required />
                        <label class="form-label" form="form2Example2">Marca</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input id="categoria" type="text" class="form-control" name="categoria" value="<?php echo $id ?>" required />
                        <label class="form-label" form="form2Example2">Categoria</label>
                    </div>

                    <button class="btn btn-primary btn-block mb-4" onclick="editarProducto()">Aceptar</button>


                    <button class="btn btn-primary btn-block mb-4" onclick="irAtras()">Atras</button>
                </form>



            </div>
        </div>

        </div>

    </main>
    <script src="editarProducto.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>

</html>