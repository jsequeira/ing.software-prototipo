<?php



?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />

    <link href="articulo.css" rel="stylesheet" />
</head>

<body>

    <header>
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white">

            <div class="container-fluid ">
                <div class="container mt-2 mb-2 me-2 ms-2">
                    <a class="navbar-brand" href="#">
                        <img src="https://mdbootstrap.com/img/logo/mdb-transaprent-noshadows.png" height="30" alt="" loading="lazy" />
                    </a>
                </div>
                <div class="d-flex justify-content-start" style="width:200%;">
                    <form class=" input-group w-auto ">
                        <select class="form-select me-1" aria-label="Default select example" style="height:45px;width:300px" ;>
                            <option selected>Categoria</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <input type="search" class="form-control" placeholder="Type query" aria-label="Search" style="height:45px;width:300px" />
                        <button class="btn btn-outline-primary" type="button" data-mdb-ripple-color="dark">
                            Search
                        </button>
                    </form>
                </div>
                <div class="btn-group me-2 ms-2" role="group">
                    <a href="login.php">
                        <button type="button" class="btn btn-primary">Login</button>
                    </a>

                </div>
            </div>
        </nav>
        <!-- Navbar -->

        <!-- Jumbotron -->
        <div class="p-5 text-center bg-light  text-white" style="background-image:url('header.png') ;">

            <h1 class="mb-3">Bienvenido a la mejor tienda en linea!</h1>
            <h4 class="mb-3">No tienes cuenta</h4>
            <a class="btn btn-primary" href="" role="button">Registrarme</a>
        </div>
        <!-- Jumbotron -->
    </header>

    <main>



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">


            <div class="modal-dialog ">
                <!-- Login Modal -->
                <div class="modal-content  rounded-0">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    </div>

                    <div class="modal-body">
                        <div class="form-outline">
                            <input type="text" id="form1" class="form-control" />
                            <label class="form-label" for="form1">Usuario</label>
                        </div>
                        <div class="form-outline mt-3">
                            <input type="password" id="form1" class="form-control" />
                            <label class="form-label" for="form1">Contraseña</label>
                        </div>

                        <div class="mt-2">
                            <p>Si no tienes cuenta puedes registrarte <a href="">>aquí</p>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
                            Close
                        </button>
                        <a href="menuUsuario.php">
                            <button type="button" class="btn btn-primary">Login</button>
                        </a>
                    </div>

                </div>
            </div>

        </div>


        <div class="container mt-5 mb-5  d-flex justify-content-end ">
            <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block ">
                <div class="card border border-primary bg-dark">
                    <img src="img/mv9.jpg" class="card-img-top" alt="..." />
                    <div class="card-body border-top  ">
                        <a href="#!" data-mdb-toggle="tooltip" title="Añadir al carrito" class="me-3"><button type="button" class="btn btn-dark" data-mdb-toggle="modal" data-mdb-target="#exampleModal"><i class="fas fa-cart-plus fa-1x"></i></button></a>

                        <a href="#!" data-mdb-toggle="tooltip" title="Lista de deseos" class="me-3"><button type="button" class="btn btn-dark" data-mdb-toggle="modal" data-mdb-target="#exampleModal"><i class="far fa-heart fa-1x"></i></button></a>

                    </div>
                </div>
            </div>
            <div class=" col-lg-4 ms-3 mb-4 mb-lg-0 d-none d-lg-block text-black ">
                <h5 class="card-title">Descripcion del producto</h5>
                <p class="card-text">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Pariatur culpa ipsam modi itaque minima, nostrum tempora odit vel ipsum doloremque fugit omnis? Obcaecati ullam laudantium itaque excepturi, doloribus accusamus facere.
                </p>
            </div>
        </div>



        <div class="container">
            <h1>Relacionado</h1>
            <div id="carouselMultiItemExample" class="carousel slide carousel-dark text-center mb-4" data-mdb-ride="carousel">
                <!-- Controls -->
                <div class="d-flex justify-content-center mb-4 ">
                    <button class="carousel-control-prev position-relative" type="button" data-mdb-target="#carouselMultiItemExample" data-mdb-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next position-relative" type="button" data-mdb-target="#carouselMultiItemExample" data-mdb-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
                <!-- Inner -->
                <div class="carousel-inner py-4">
                    <!-- Single item -->
                    <div class="carousel-item ">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <img src="img/mv1.jpg" class="card-img-top" 840 × 840 alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv2.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv3.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Single item -->
                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="card">
                                        <img src="img/mv4.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv5.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv6.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="articulo.php" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Single item -->
                    <div class="carousel-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                                    <div class="card">
                                        <img src="img/mv7.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv8.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                    <div class="card">
                                        <img src="img/mv9.jpg" class="card-img-top" alt="..." />
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn-primary">Button</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Inner -->
            </div>
        </div>

    </main>

    <footer class="bg-primary text-center text-white border ">
        <!-- Grid container -->
        <div class="container p-4 pb-0 ">
            <!-- Section: Social media -->
            <section class="mb-4  ">
                <!-- Facebook -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #3b5998;" href="#!" role="button"><i class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #55acee;" href="#!" role="button"><i class="fab fa-twitter"></i></a>

                <!-- Google -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #dd4b39;" href="#!" role="button"><i class="fab fa-google"></i></a>

                <!-- Instagram -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #ac2bac;" href="#!" role="button"><i class="fab fa-instagram"></i></a>

                <!-- Linkedin -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #0082ca;" href="#!" role="button"><i class="fab fa-linkedin-in"></i></a>

            </section>
            <!-- Section: Social media -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3 " style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
</body>

</html>