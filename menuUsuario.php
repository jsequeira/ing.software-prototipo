<?php
include_once 'conexion.php';




/* -------------------------------------------------------------------------- */
/*                       ELIMINAR CATEGORIAS                                  */
/* -------------------------------------------------------------------------- */
if ($_GET['action'] == "eliminarCategoria") {
    $nombreCat = $_GET["nombreCat"];
    echo  $nombreCat;
    $query = 'DELETE FROM categoria WHERE nombre = ?';
    $resultado = conexionCover()->prepare($query);
    $resultado->execute(array($nombreCat));
}

/* -------------------------------------------------------------------------- */
/*                       SELECCIONA TODAS LAS CATEGORIAS                      */
/* -------------------------------------------------------------------------- */


$query = 'SELECT * FROM categoria';
$resultado = conexionCover()->prepare($query);
$resultado->execute();
$categorias = $resultado->fetchAll();
/* -------------------------------------------------------------------------- */
/*                       Eliminar PRODUCTOS                                   */
/* -------------------------------------------------------------------------- */

if ($_GET['action'] == "eliminarProducto") {
    $id = $_GET["id"];

    $query = 'DELETE FROM Producto WHERE id = ?';
    $resultado = conexionCover()->prepare($query);
    $resultado->execute(array($id));
}
/* -------------------------------------------------------------------------- */
/*                       SELECCIONA TODOS LOS PRODUCTOS                       */
/* -------------------------------------------------------------------------- */

$query = 'SELECT * FROM producto';
$resultado = conexionCover()->prepare($query);
$resultado->execute(array($email, $password));
$productos = $resultado->fetchAll();



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="menuUsuario.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light  border-bottom " style="background-color: #1565C0;">
        <div class="container-fluid d-flex d-flex justify-content-end">
            <ul class="navbar-nav ">
                <!-- Avatar -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle d-flex align-items-center" href="#" id="navbarDropdownMenuLink" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                        <img src="https://mdbootstrap.com/img/Photos/Avatars/img (31).jpg" class="rounded-circle" height="22" alt="" loading="lazy" />
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end " aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="#">My profile</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="index.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div class="main mt-4 ">
        <div class="row">
            <div class="col-2">
                <!-- Tab navs -->
                <div class="nav flex-column nav-tabs text-center" id="v-tabs-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-tabs-home-tab" data-mdb-toggle="tab" href="#v-tabs-home" role="tab" aria-controls="v-tabs-home" aria-selected="true">Home</a>
                    <a class="nav-link" id="v-tabs-productos-tab" data-mdb-toggle="tab" href="#v-tabs-productos" role="tab" aria-controls="v-tabs-productos" aria-selected="false">Mi carrito</a>

                </div>
                <!-- Tab navs -->
            </div>

            <div class="col-10 border-start">
                <!-- Tab content -->
                <div class="tab-content" id="v-tabs-tabContent">
                    <div class="tab-pane fade show active" id="v-tabs-home" role="tabpanel" aria-labelledby="v-tabs-home-tab">
                        <div class="container d-flex flex-sm-wrap">
                            <div class="card border  border-primary" style="width: 18rem">
                                <div class="card-body">
                                    <h5 class="card-title">Mi carrito</h5>
                                    <p class="card-text">
                                        Podras ver todos los productos existentes del sistema; Eliminar,editar,crear Productos.
                                    </p>

                                    <button class="btn btn-outline-primary" id="v-tabs-productos-tab#1" onclick="seleccionarTab(this.id)"> ir</button>

                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-tabs-productos" role="tabpanel" aria-labelledby="v-tabs-producto-tab">

                        <div class="container">

                            <div class="container scrollcontainer border  mt-3 mb-3">


                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Spied element -->
                                        <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example d-flex flex-wrap flex-row">

                                            <div class="card mt-3 mb-3 me-4 ms-4 border" style="width: 16.5rem; height:22.5rem;">
                                                <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" width: 17rem; height:17rem; class="card-img-top" alt="..." />
                                                <div class="card-body">
                                                    <div class="d-flex row">
                                                        <div>
                                                            <h5 class="card-title">Titulo</h5>
                                                        </div>
                                                        <div class="card-title">
                                                            <h6 class="card-title">Precio</h6>
                                                        </div>
                                                    </div>
                                                    <p class="card-text">
                                                        Descripcion...
                                                    </p>
                                                    <a href="articulo.php" class="btn btn-primary">Ver</a>
                                                </div>
                                            </div>

                                            <!-- Spied element -->
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-tabs-categorias" role="tabpanel" aria-labelledby="v-tabs-messages-tab">
                        <div class="container">
                            <table class="table align-middle">

                                <thead>
                                    <tr>
                                        <th scope="col">Categoria</th>
                                        <th scope="col">Editar</th>
                                        <th scope="col">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($categorias as $categoria) :

                                    ?>
                                        <tr id="<?php echo  $categoria['nombre'] ?>">
                                            <td><?php echo $categoria['nombre'] ?></td>
                                            <td>
                                                <a href="editarCategoria.php?categoria=<?php echo $categoria['nombre'] ?>">
                                                    <button id="<?php echo  '#', $categoria['nombre'] ?>" type="button" class="btn btn-danger btn-sm px-3 ">
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                </a>
                                            </td>
                                            <td>
                                                <button id="<?php echo  '#', $categoria['nombre'] ?>" class="btn btn-danger btn-sm px-3" onclick="eliminarCategoria(this.id)">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-tabs-ventas" role="tabpanel" aria-labelledby="v-tabs-ventas-tab">
                        Ventas
                    </div>
                </div>
                <!-- Tab content -->
            </div>
        </div>
    </div>
    <footer class="text-center text-white fixed-bottom" style="background-color: #82B1FF;">
        <!-- Grid container -->
        <div class="container p-4"></div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: #1565C0;">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">Proyecto ing.en software</a>
        </div>
        <!-- Copyright -->
    </footer>
    <script src="menuUsuario.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?php
    if ($_GET['action'] == "editarCategoria") {

        echo '<script type="text/javascript">

        elementoTab = document.getElementById("v-tabs-categorias-tab");

        elementoDiv = elementoTab.getAttribute("href");
        elementoDiv = elementoDiv.replace("#", "");
      
     
      
        elementoDivHome = document.getElementById("v-tabs-home");
        elementoDivHome.setAttribute("class", "tab-pane fade");
      
        elementoDiv = document.getElementById(elementoDiv);
      
        elementoDiv.setAttribute("class", "tab-pane fade active show");
      
        elementoHome = document.getElementById("v-tabs-home-tab");
        elementoHome.setAttribute("class", "nav-link");
        elementoHome.setAttribute("aria-selected", "false");
      
        elementoTab.setAttribute("class", "nav-link active");
        elementoTab.setAttribute("aria-selected", "true");

    </script>';
    }
    if ($_GET['action'] == "editarProducto") {

        echo '<script type="text/javascript">

        elementoTab = document.getElementById("v-tabs-productos-tab");

        elementoDiv = elementoTab.getAttribute("href");
        elementoDiv = elementoDiv.replace("#", "");
      
     
      
        elementoDivHome = document.getElementById("v-tabs-home");
        elementoDivHome.setAttribute("class", "tab-pane fade");
      
        elementoDiv = document.getElementById(elementoDiv);
      
        elementoDiv.setAttribute("class", "tab-pane fade active show");
      
        elementoHome = document.getElementById("v-tabs-home-tab");
        elementoHome.setAttribute("class", "nav-link");
        elementoHome.setAttribute("aria-selected", "false");
      
        elementoTab.setAttribute("class", "nav-link active");
        elementoTab.setAttribute("aria-selected", "true");

    </script>';
    }
    ?>
</body>

</html>