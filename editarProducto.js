function editarProducto() {
  alert('asddas');
  var id = document.getElementById("id").value;
  var codigo = document.getElementById("codigo").value;
  var nombre = document.getElementById("nombre").value;
  var precioCompra = document.getElementById("precioCompra").value;
  var precioVenta = document.getElementById("precioVenta").value;
  var stock = document.getElementById("stock").value;
  var marca = document.getElementById("marca").value;
  var categoria = document.getElementById("categoria").value;

  if (codigo) {
    var parametros = {
      action: "editar",
      id: id,
      codigo: codigo,
      nombre: nombre,
      precio_compra: precioCompra,
      precio_venta: precioVenta,
      stock: stock,
      marca: marca,
      categoria: categoria
    };
    $.ajax({
      data: parametros,
      url: "editarProducto.php",
      type: "get",
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor");
      },
      success: function (response) {
        $("#resultado").html(response);

        window.location = "menuAdministrador.php?action=editarProducto";
      },
    });
  } else {
    alert("Digite la nueva categoria");
  }
}

function irAtras() {
  window.history.back();
  alert("atras");
}
