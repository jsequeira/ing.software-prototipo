function irAtras() {
  window.history.back();
  alert("atras");
}

function editarCategoria() {
  var categoriaVieja = document.getElementById("categoriaVieja").value;
  var categoriaNueva = document.getElementById("categoriaNueva").value;

  if (categoriaNueva) {
    var parametros = {
      action: "editar",
      "categoriaVieja": categoriaVieja,
      "categoriaNueva": categoriaNueva
    };
    $.ajax({
      data: parametros,
      url: "editarCategoria.php",
      type: "get",
      beforeSend: function () {
        $("#resultado").html("Procesando, espere por favor");
      },
      success: function (response) {
        $("#resultado").html(response);

        window.location = "menuAdministrador.php?action=editarCategoria";
      },
    });
  } else {
    alert("Digite la nueva categoria");
  }
}
