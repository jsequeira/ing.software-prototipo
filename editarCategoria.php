<?php
include_once 'conexion.php';

$categoriaVieja = $_GET['categoria'];


/* -------------------------------------------------------------------------- */
/*                       EDITAR CATEGORIAS                                  */
/* -------------------------------------------------------------------------- */
if ($_GET['action']=='editar') {

    $categoriaVieja = $_GET["categoriaVieja"];
    $categoriaNueva = $_GET["categoriaNueva"];
    echo $categoriaVieja;
    echo $categoriaNuevo;
    $query = 'UPDATE categoria SET nombre=? WHERE nombre = ?';
    $resultado = conexionCover()->prepare($query);
    $resultado->execute(array($categoriaNueva , $categoriaVieja));
  
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="editarCategoria.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
</head>


<body >
    <main>
        <div class="container-form mt-1 mb-1">
            <div class="border p-5 border-primary rounded">

                <div class="form-outline mb-4">
                    <input id="categoriaVieja" readonly type="text" value="<?php echo $categoriaVieja ?>" id="form2Example1" class="form-control" name="categoriaVieja" />
                </div>
              

                <div class="form-outline mb-4">
                    <input id="categoriaNueva"  type="text" class="form-control" name="categoriaNuevo" required />
                    <label class="form-label" form="form2Example2">Categoria Nueva</label>
                </div>


                <button  class="btn btn-primary btn-block mb-4" onclick="editarCategoria()" >Aceptar</button>


                <button   class="btn btn-primary btn-block mb-4" onclick="irAtras()">Atras</button>

            </div>
        </div>
    </main>
    <script src="editarCategoria.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>

</html>